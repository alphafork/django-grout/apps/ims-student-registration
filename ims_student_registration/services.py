from datetime import date
from random import randrange

from django.db.models import Value
from django.db.models.functions import Concat
from ims_workday.services import add_workdays_to_date
from reusable_models import get_model_from_string

from ims_student_registration.models import BatchStudent

DailySchedule = get_model_from_string("DAILY_SCHEDULE")


def registration_no_generator():
    return randrange(1, 10000)


def get_student_registration_data(registration):
    student = {
        "id": registration.student.user.id,
        "name": registration.student.user.get_full_name(),
        "email": registration.student.user.email,
        "applied_date": registration.applied_date,
        "exit_date": registration.exit_date,
        "student_no": registration.student.student_no,
    }
    registration = {
        "id": registration.id,
        "number": registration.registration_no,
        "remarks": registration.remarks,
        "status": dict(registration.REGISTRATION_STATUS)[registration.status],
    }
    return {
        "student": student,
        "registration": registration,
    }


def get_batch_data(registration):
    batch_student_list = BatchStudent.objects.filter(registration=registration)
    batches = []
    for batch_student in batch_student_list:
        batches.append(
            {
                "batch_student": batch_student.id,
                "batch": batch_student.batch.id,
                "batch_name": batch_student.batch.name,
                "course": batch_student.batch.course.id,
                "course_name": batch_student.batch.course.name,
                "applied_date": batch_student.registration.applied_date,
                "status": dict(batch_student.BATCH_STUDENT_STATUS)[
                    batch_student.status
                ],
                "enrolled_date": batch_student.enrolled_date,
            }
        )
    return {"batches": batches}


def get_active_batch_data(registration):
    batch_student = BatchStudent.objects.filter(
        registration=registration,
    ).exclude(status="mi")
    batch = None
    if batch_student.exists():
        batch_student = batch_student.first()
        batch = {
            "batch_student": batch_student.id,
            "batch": batch_student.batch.id,
            "batch_name": batch_student.batch.name,
            "course": batch_student.batch.course.id,
            "course_name": batch_student.batch.course.name,
            "applied_date": batch_student.registration.applied_date,
            "status": dict(batch_student.BATCH_STUDENT_STATUS)[batch_student.status],
            "enrolled_date": batch_student.enrolled_date,
        }
        batch["todays_schedule"] = (
            DailySchedule.objects.filter(
                date=date.today(),
                schedule__batch=batch_student.batch,
            )
            .annotate(
                faculty_name=Concat(
                    "faculty__staff__user__first_name",
                    Value(" "),
                    "faculty__staff__user__last_name",
                )
            )
            .values("slot", "faculty_name")
            or None
        )
    return {"batch": batch}


def get_course_data(registration):
    coursestudent = getattr(registration, "coursestudent", None)
    if coursestudent:
        batch_student_list = BatchStudent.objects.filter(registration=registration)
        start_date, end_date = None, None
        if batch_student_list:
            start_date = (
                batch_student_list.order_by("enrolled_date").first().enrolled_date
            )
            if coursestudent.course.duration_days:
                end_date = add_workdays_to_date(
                    start_date, coursestudent.course.duration_days
                )
        course = {
            "id": coursestudent.course.id,
            "name": coursestudent.course.name,
            "start_date": start_date,
            "end_date": end_date,
            **get_active_batch_data(registration),
            **get_enrollment_status(registration),
        }
        return {"course": course}
    return {"course": None}


def get_enrollment_status(registration):
    if BatchStudent.objects.filter(registration=registration).exists():
        return {"enrollment_status": "all"}
    return {"enrollment_status": "none"}
