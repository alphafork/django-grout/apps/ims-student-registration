from datetime import datetime

from ims_base.serializers import BaseModelSerializer
from ims_user.services import get_contact_data, get_profile_data
from rest_framework import serializers
from reusable_models import get_model_from_string

from .models import BatchStudent, CourseStudent, StudentRegistration
from .services import (
    get_course_data,
    get_student_registration_data,
    registration_no_generator,
)

Student = get_model_from_string("STUDENT")
Batch = get_model_from_string("BATCH")


class CourseStudentSerializer(BaseModelSerializer):
    student = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=Student.objects.all()
    )

    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["registration"]
        extra_meta = {
            "student": {
                "related_model_url": "/student/student",
            },
        }

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        student = validated_data["student"]
        course = validated_data["course"]
        user = self.context["request"].user
        if (
            not user.groups.filter(name__in=["Admin", "Manager", "Accountant"]).exists()
            and student.user != user
        ):
            raise serializers.ValidationError({"student": "Student value not allowed."})
        student_registrations = StudentRegistration.objects.filter(student=student)
        if CourseStudent.objects.filter(
            course=course, registration__in=student_registrations
        ).exists():
            raise serializers.ValidationError(
                {"course": "You have already applied for this course."}
            )
        return validated_data

    def create(self, validated_data):
        student_registration = StudentRegistration(
            registration_no=registration_no_generator(),
            student=validated_data.pop("student"),
            applied_date=datetime.now().strftime("%Y-%m-%d"),
            status="ap",
        )
        student_registration.save()
        validated_data["registration"] = student_registration
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data.update(get_student_registration_data(instance.registration))
        return data


class BatchStudentSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["status"]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        registration = validated_data["registration"]
        batch = validated_data["batch"]
        if batch.course.id in registration.batchstudent_set.values_list(
            "batch__course", flat=True
        ):
            raise serializers.ValidationError(
                {"batch": f"{registration} is already doing course {batch.course}."}
            )
        if BatchStudent.objects.filter(
            registration__student=registration.student, batch=batch
        ).exists():
            raise serializers.ValidationError(
                {"registration": f"{registration} is already in batch {batch}."}
            )
        validated_data["status"] = "ac"
        return validated_data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["status"] = instance.status
        data.update(get_student_registration_data(instance.registration))
        return data


class StudentRegistrationSerializer(BaseModelSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)

        if (
            not self.context["request"]
            .user.groups.filter(name__in=["Manager", "Admin", "Accountant"])
            .exists()
            and self.instance.status != "ap"
        ):
            raise serializers.ValidationError({"status": "Action not allowed."})
        return validated_data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data.update(
            {
                **get_student_registration_data(instance),
                **get_course_data(instance),
            }
        )
        return data


class StudentBatchMigrationSerializer(BaseModelSerializer):
    batch_student = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=BatchStudent.objects.all(),
    )
    new_batch = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=Batch.objects.all(),
    )

    class Meta(BaseModelSerializer.Meta):
        model = BatchStudent
        fields = ["batch_student", "new_batch"]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        validation_errors = []
        batch_student = validated_data["batch_student"]
        registration = batch_student.registration
        student = registration.student.user.get_full_name()
        new_batch = validated_data["new_batch"]
        course = new_batch.course
        coursestudent = getattr(registration, "coursestudent", None)
        if coursestudent and coursestudent.course != course:
            validation_errors.append(
                {
                    "batch_student": (
                        f"{student} | {registration}"
                        + f" is not applied for course {course}"
                    )
                }
            )
        if BatchStudent.objects.filter(
            batch=new_batch, registration=registration
        ).exists():
            validation_errors.append(
                {
                    "new_batch": (
                        f"{student} | {registration}"
                        + " already joined for batch {new_batch}"
                    )
                }
            )

        if validation_errors:
            raise serializers.ValidationError(validation_errors)

        return validated_data

    def create(self, validated_data):
        old_batch_student = validated_data.pop("batch_student")
        old_batch_student.status = "mi"
        old_batch_student.save()

        new_batch = validated_data.pop("new_batch")
        validated_data.update(
            {
                "registration": old_batch_student.registration,
                "enrolled_date": datetime.now().date(),
                "batch": new_batch,
                "status": "ac",
            }
        )
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["enrolled_date"] = instance.enrolled_date
        data["status"] = instance.status
        data.update(get_student_registration_data(instance.registration))
        return data


class BatchMigrationSerializer(BaseModelSerializer):
    batch_student_migration = serializers.ListField(
        write_only=True,
        child=StudentBatchMigrationSerializer(),
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["batch_student_migration"]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        batch_student_migration = validated_data.get("batch_student_migration", [])
        if not batch_student_migration:
            raise serializers.ValidationError(
                {"batch_student_migration": "This field can not be empty."}
            )
        return validated_data

    def create(self, validated_data):
        batch_student_migration = validated_data.pop("batch_student_migration", [])

        for student_migration in batch_student_migration:
            old_batch_student = dict(student_migration)["batch_student"]
            batch_student = {
                "registration": old_batch_student.registration,
                "enrolled_date": datetime.now().date(),
                "batch": dict(student_migration)["new_batch"],
                "status": "ac",
            }

            BatchStudent.objects.create(**dict(batch_student))
            old_batch_student.status = "mi"
            old_batch_student.save()

        return True


class StudentRegistrationDetailsSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = Student

    def to_representation(self, instance):
        student_registrtion = StudentRegistration.objects.filter(student=instance)
        return {
            **get_profile_data(instance.user),
            **get_contact_data(instance.user),
            "has_applied_any": student_registrtion.exists(),
        }
