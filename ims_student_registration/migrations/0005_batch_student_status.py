# Generated by Django 4.1.9 on 2023-12-12 08:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ims_student_registration", "0004_batch_student_exit_date"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="batchstudent",
            name="is_active",
        ),
        migrations.AddField(
            model_name="batchstudent",
            name="status",
            field=models.CharField(
                choices=[
                    ("ac", "active"),
                    ("mi", "migrated"),
                    ("di", "disconitnued"),
                    ("co", "completed"),
                ],
                default="ac",
                max_length=2,
            ),
        ),
    ]
