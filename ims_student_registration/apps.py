from django.apps import AppConfig


class IMSStudentRegistrationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_student_registration"

    model_strings = {
        "BATCH_STUDENT": "BatchStudent",
        "COURSE_STUDENT": "CourseStudent",
        "STUDENT_REGISTRATION": "StudentRegistration",
        "REGISTRATION": "StudentRegistration",
    }
