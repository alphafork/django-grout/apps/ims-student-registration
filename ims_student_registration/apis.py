from django.db.models import Q
from ims_api_permission.apis import APIMethodPermission
from ims_base.apis import BaseAPIView, BaseAPIViewSet
from ims_student.apis import StudentDetailAPI
from ims_user.apis import UserFilter
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import BaseFilterBackend
from rest_framework.response import Response
from reusable_models import get_model_from_string

from ims_student_registration.serializers import (
    BatchMigrationSerializer,
    StudentBatchMigrationSerializer,
    StudentRegistrationDetailsSerializer,
    StudentRegistrationSerializer,
)

from .models import BatchStudent, CourseStudent, StudentRegistration

Student = get_model_from_string("STUDENT")
Course = get_model_from_string("COURSE")


class StudentRegistrationFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "registration_id" in request.query_params:
            filter_map = {"id": request.query_params["registration_id"]}
        elif "registration_no" in request.query_params:
            filter_map = {"registration_no": request.query_params["registration_no"]}
        elif "student_id" in request.query_params:
            filter_map = {"student_id": request.query_params["student_id"]}
        elif "student_no" in request.query_params:
            filter_map = {"student__student_no": request.query_params["student_no"]}
        elif "student_name" in request.query_params:
            filter_map = {
                "student__user__first_name__icontains": request.query_params[
                    "student_name"
                ]
            }
        if "status" in request.query_params:
            filter_map.update({"status": request.query_params["status"]})
        return queryset.filter(**filter_map)


class StudentSearchFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        search_query = request.query_params.get("student_search", None)
        if search_query:
            filter_queryset = Q()
            search_fields = [
                "registration_no",
                "student__student_no",
                "student__user__first_name",
                "student__user__last_name",
            ]
            for field in search_fields:
                filter_queryset |= Q(**{f"{field}__icontains": search_query})
            return queryset.filter(filter_queryset)
        return queryset


class StudentDetailsSearchFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        search_query = request.query_params.get("student_search", None)
        if search_query:
            filter_queryset = Q()
            search_fields = [
                "student_no",
                "user__first_name",
                "user__last_name",
            ]
            for field in search_fields:
                filter_queryset |= Q(**{f"{field}__icontains": search_query})
            return queryset.filter(filter_queryset)
        return queryset


class StudentEnrollmentFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "has_enrolled_any" in request.query_params:
            course_registrations = CourseStudent.objects.exclude(
                registration__status="ap"
            ).values_list("registration", flat=True)
            batch_registrations = BatchStudent.objects.filter(
                registration__in=course_registrations
            ).values_list("registration", flat=True)
            if request.query_params["has_enrolled_any"] == "True":
                filter_map = {"id__in": batch_registrations}
            elif request.query_params["has_enrolled_any"] == "False":
                non_batch_registrations = []
                for registration in course_registrations:
                    if registration not in batch_registrations:
                        non_batch_registrations.append(registration)
                filter_map = {"id__in": non_batch_registrations}
        return queryset.filter(**filter_map)


class StudentBatchFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "batch_id" in request.query_params:
            batch_student_registrations = BatchStudent.objects.filter(
                **{"batch": request.query_params["batch_id"]},
            ).values("registration_id")
            return queryset.filter(**{"id__in": batch_student_registrations})
        return queryset


class StudentCourseFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        filter_map = {}
        if "course_id" in request.query_params:
            course_student_registrations = CourseStudent.objects.filter(
                course=request.query_params["course_id"]
            ).values("registration_id")
            filter_map = {"id__in": course_student_registrations}
        return queryset.filter(**filter_map)


class CourseStudentAPI(BaseAPIViewSet):
    model_class = CourseStudent
    lookup_field = "registration"
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Accountant", "Manager", "Faculty", "Student"],
        "GET": ["Admin", "Accountant", "Manager", "Accountant", "Faculty", "Student"],
        "POST": ["Admin", "Accountant", "Manager", "Faculty", "Student"],
        "DELETE": ["Admin"],
    }

    def get_serializer_context(self):
        serializer_context = super().get_serializer_context()
        serializer_context["student_id"] = self.kwargs.get(self.lookup_field, None)
        return serializer_context

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request.user.is_superuser or self.request.user.groups.filter(
            name__in=["Admin", "Manager", "Accountant"]
        ):
            filter_kwargs = {}
        elif self.request.user.groups.filter(name="Faculty"):
            filter_kwargs = {
                "course__subjects__faculties__staff__user": self.request.user
            }
        else:
            registrations = StudentRegistration.objects.filter(
                student_id=self.request.user.id,
            )
            filter_kwargs = {"registration__in": registrations}
        return queryset.filter(**filter_kwargs)

    def perform_destroy(self, instance):
        if instance.registration.status == "ap":
            instance.registration.delete()
            return super().perform_destroy(instance)
        raise PermissionDenied


class StudentRegistrationAPI(BaseAPIViewSet):
    model_class = StudentRegistration
    serializer_class = StudentRegistrationSerializer
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
        StudentSearchFilter,
        StudentRegistrationFilter,
        StudentCourseFilter,
        StudentBatchFilter,
        StudentEnrollmentFilter,
    ]
    obj_user_groups = ["Admin", "Manager", "Accountant", "Faculty"]
    user_lookup_field = "student__user"
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager", "Accountant", "Faculty", "Student"],
        "GET": ["Admin", "Manager", "Accountant", "Faculty", "Student"],
        "POST": ["Admin", "Manager", "Accountant", "Faculty"],
        "PUT": ["Admin", "Manager", "Accountant", "Faculty"],
        "PATCH": ["Admin", "Manager", "Accountant", "Faculty"],
        "DELETE": ["Admin"],
    }


class GenericStudentDetailsAPIView(BaseAPIView):
    def get(self, request, **kwargs):
        pk = kwargs.get("pk")
        return Response(
            {
                "results": self.get_response_data(pk),
            }
        )

    def get_response_data(self, pk):
        pass


class StudentCourseListAPI(GenericStudentDetailsAPIView):
    def get_response_data(self, student_id):
        courses = Course.objects.values()
        data = []
        for course in courses:
            course_student = CourseStudent.objects.filter(
                registration__student_id=student_id, course_id=course["id"]
            )
            course["status"] = "unregistered"
            if course_student.exists():
                registration = course_student[0].registration
                course["registration_id"] = registration.id
                course["status"] = dict(registration.REGISTRATION_STATUS)[
                    registration.status
                ]
            data.append(course)
        return data


class StudentBatchMigrationAPI(BaseAPIViewSet):
    model_class = BatchStudent
    serializer_class = StudentBatchMigrationSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager"],
        "GET": ["Admin", "Manager"],
        "POST": ["Admin", "Manager"],
        "PUT": ["Admin", "Manager"],
        "PATCH": ["Admin", "Manager"],
    }


class BatchMigrationAPI(BaseAPIViewSet):
    model_class = BatchStudent
    serializer_class = BatchMigrationSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Manager"],
        "GET": ["Admin", "Manager"],
        "POST": ["Admin", "Manager"],
        "PUT": ["Admin", "Manager"],
        "PATCH": ["Admin", "Manager"],
    }


class StudentRegistrationDetailsAPI(StudentDetailAPI):
    serializer_class = StudentRegistrationDetailsSerializer
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
        StudentDetailsSearchFilter,
    ]
