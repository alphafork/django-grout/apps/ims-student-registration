from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import (
    BatchMigrationAPI,
    CourseStudentAPI,
    StudentBatchMigrationAPI,
    StudentCourseListAPI,
    StudentRegistrationAPI,
    StudentRegistrationDetailsAPI,
)

router = routers.SimpleRouter()
router.register(r"student-application", CourseStudentAPI, "course-apply")
router.register(
    r"student-batch-migration", StudentBatchMigrationAPI, "student-batch-migration"
)
router.register(r"batch-migration", BatchMigrationAPI, "batch-migration")
router.register(r"student-details", StudentRegistrationAPI, "student-details")
router.register(r"students", StudentRegistrationDetailsAPI, "students")

urlpatterns = [
    path("courses/<int:pk>/", StudentCourseListAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
