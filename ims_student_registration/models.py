from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from ims_base.models import AbstractLog
from reusable_models import get_model_from_string

Batch = get_model_from_string("BATCH")


class StudentRegistration(AbstractLog):
    REGISTRATION_STATUS = [
        ("ap", "applied"),
        ("po", "processing"),
        ("pr", "approved"),
        ("co", "completed"),
        ("di", "discontinued"),
        ("de", "deregistered"),
    ]
    registration_no = models.CharField(max_length=255)
    student = models.ForeignKey(
        get_model_from_string("STUDENT"), on_delete=models.CASCADE
    )
    applied_date = models.DateField()
    exit_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=REGISTRATION_STATUS, default="ap")
    remarks = models.TextField(blank=True)

    def __str__(self):
        return f"{self.student.__str__()} | {self.registration_no}"


class CourseStudent(AbstractLog):
    registration = models.OneToOneField(
        StudentRegistration,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    course = models.ForeignKey(
        get_model_from_string("COURSE"), on_delete=models.CASCADE
    )

    def __str__(self):
        return self.registration.__str__()


class BatchStudent(AbstractLog):
    BATCH_STUDENT_STATUS = [
        ("ac", "active"),
        ("mi", "migrated"),
        ("di", "disconitnued"),
        ("co", "completed"),
    ]
    registration = models.ForeignKey(StudentRegistration, on_delete=models.CASCADE)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    enrolled_date = models.DateField()
    exit_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=BATCH_STUDENT_STATUS, default="ac")

    def __str__(self):
        return f"{self.registration.__str__()} | {self.batch.__str__()}"


@receiver(post_save, sender=Batch)
def update_batchstudent_status_on_batch_completion(instance, **kwargs):
    if instance.status == "co":
        batch_students = BatchStudent.objects.filter(batch=instance, status="ac")
        for batch_student in batch_students:
            batch_student.status = "co"
            batch_student.exit_date = instance.end_date
            batch_student.save()


@receiver(post_save, sender=BatchStudent)
def update_registration_status_on_program_completion(instance, **kwargs):
    course_student = getattr(instance.registration, "coursestudent", None)
    if (
        course_student
        and instance.status == "co"
        and not instance.registration.batchstudent_set.filter(status="ac").exists()
    ):
        if course_student:
            course_student.registration.status = "co"
            course_student.registration.save()
